package com.sashalykke.gotcharacters.utils;

import android.app.Application;

import io.realm.Realm;

public class GOTCharApp extends Application {

    private static final String TAG = ConstantManager.TAG_PREFIX+"Application";

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
    }

}
