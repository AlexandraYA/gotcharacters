package com.sashalykke.gotcharacters.ui.activities;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.util.SortedList;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.sashalykke.gotcharacters.R;
import com.sashalykke.gotcharacters.data.network.api.ApiService;
import com.sashalykke.gotcharacters.data.network.restmodels.GOTHouse;
import com.sashalykke.gotcharacters.data.storage.models.House;
import com.sashalykke.gotcharacters.ui.fragments.LannisterFragment;
import com.sashalykke.gotcharacters.ui.fragments.StarkFragment;
import com.sashalykke.gotcharacters.ui.fragments.TargarienFragment;
import com.sashalykke.gotcharacters.utils.GOTCharApp;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends BasicActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar mToolbar;
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle mToggle;
    private TabLayout mTabs;
    private Adapter mAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.app_name, R.string.app_name);
        mDrawer.addDrawerListener(mToggle);
        mToggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(mViewPager);
        // Set Tabs inside Toolbar
        mTabs = (TabLayout) findViewById(R.id.tabs);
        mTabs.setupWithViewPager(mViewPager);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://anapioficeandfire.com/api/houses/")
                .addConverterFactory(GsonConverterFactory.create()).build();

        ApiService service = retrofit.create(ApiService.class);
        Call<GOTHouse> call = service.loadHouseStark();
        call.enqueue(new Callback<GOTHouse>() {

            @Override
            public void onResponse(Call<GOTHouse> call, Response<GOTHouse> response) {
                Log.d("MyNetwork", response.body().getName());
                Log.d("MyNetwork", response.body().getWords());

                House house = new House();
                String url = response.body().getUrl();
                int index = url.lastIndexOf("/");
                String id = url.substring(index+1);

                house.setId( id );
                house.setName( response.body().getName() );
                house.setWords( response.body().getWords() );
                house.setUrl( url );

                // Persist your data in a transaction
                GOTCharApp.sRealm.beginTransaction();
                GOTCharApp.sRealm.copyToRealm(house);
                GOTCharApp.sRealm.commitTransaction();
            }

            @Override
            public void onFailure(Call<GOTHouse> call, Throwable t) {

            }
        });

        Call<GOTHouse> call2 = service.loadHouseLannister();
        call.enqueue(new Callback<GOTHouse>() {

            @Override
            public void onResponse(Call<GOTHouse> call, Response<GOTHouse> response) {
                Log.d("MyNetwork", response.body().getName());
                Log.d("MyNetwork", response.body().getWords());

                House house = new House();
                String url = response.body().getUrl();
                int index = url.lastIndexOf("/");
                String id = url.substring(index+1);

                house.setId( id );
                house.setName( response.body().getName() );
                house.setWords( response.body().getWords() );
                house.setUrl( url );

                // Persist your data in a transaction
                GOTCharApp.sRealm.beginTransaction();
                GOTCharApp.sRealm.copyToRealm(house);
                GOTCharApp.sRealm.commitTransaction();
            }

            @Override
            public void onFailure(Call<GOTHouse> call, Throwable t) {

            }
        });

        Call<GOTHouse> call3 = service.loadHouseTargaryen()

                ;
        call.enqueue(new Callback<GOTHouse>() {

            @Override
            public void onResponse(Call<GOTHouse> call, Response<GOTHouse> response) {
                Log.d("MyNetwork", response.body().getName());
                Log.d("MyNetwork", response.body().getWords());

                House house = new House();
                String url = response.body().getUrl();
                int index = url.lastIndexOf("/");
                String id = url.substring(index+1);

                house.setId( id );
                house.setName( response.body().getName() );
                house.setWords( response.body().getWords() );
                house.setUrl( url );

                // Persist your data in a transaction
                GOTCharApp.sRealm.beginTransaction();
                GOTCharApp.sRealm.copyToRealm(house);
                GOTCharApp.sRealm.commitTransaction();
            }

            @Override
            public void onFailure(Call<GOTHouse> call, Throwable t) {

            }
        });
    }

    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {
        mAdapter = new Adapter(getSupportFragmentManager());
        mAdapter.addFragment(new LannisterFragment(), "Ланнистеры");
        mAdapter.addFragment(new StarkFragment(), "Старки");
        mAdapter.addFragment(new TargarienFragment(), "Таргариены");
        viewPager.setAdapter(mAdapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.stark_home) {
            showToast("Старки");
            mViewPager.setCurrentItem(1);
        } else if (id == R.id.lannister_home) {
            showToast("Ланнистеры");
            mViewPager.setCurrentItem(0);
        } else if (id == R.id.targarien_home) {
            showToast("Таргарианы");
            mViewPager.setCurrentItem(2);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
