package com.sashalykke.gotcharacters.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sashalykke.gotcharacters.R;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }
}
