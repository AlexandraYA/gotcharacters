package com.sashalykke.gotcharacters.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.sashalykke.gotcharacters.utils.ConstantManager;

public class SplashActivity extends BasicActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, ConstantManager.SPLASH_TIME_OUT);
    }
}
