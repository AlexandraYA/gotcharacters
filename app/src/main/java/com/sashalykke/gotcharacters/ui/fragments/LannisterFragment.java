package com.sashalykke.gotcharacters.ui.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sashalykke.gotcharacters.R;

public class LannisterFragment extends Fragment{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(
                R.layout.recycler_view, container, false);
        LannisterFragment.ContentAdapter adapter = new LannisterFragment.ContentAdapter(recyclerView.getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return recyclerView;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.fragment_lannister, parent, false));
            name = (TextView) itemView.findViewById(R.id.list_text);
        }
    }
    /**
     * Adapter to display recycler view.
     */
    public static class ContentAdapter extends RecyclerView.Adapter<LannisterFragment.ViewHolder> {
        // Set numbers of List in RecyclerView.
        private static final int LENGTH = 18;
        private final String[] mHouses;

        public ContentAdapter(Context context) {
            Resources resources = context.getResources();
            mHouses = resources.getStringArray(R.array.houses);
        }

        @Override
        public LannisterFragment.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new LannisterFragment.ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(LannisterFragment.ViewHolder holder, int position) {
            holder.name.setText(mHouses[position % mHouses.length]);
        }

        @Override
        public int getItemCount() {
            return LENGTH;
        }
    }
}
