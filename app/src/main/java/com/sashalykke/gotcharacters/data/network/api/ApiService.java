package com.sashalykke.gotcharacters.data.network.api;

import com.sashalykke.gotcharacters.data.network.restmodels.GOTHouse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    @GET("362")
    Call<GOTHouse> loadHouseStark();

    @GET("229")
    Call<GOTHouse> loadHouseLannister();

    @GET("378")
    Call<GOTHouse> loadHouseTargaryen();
    
}
