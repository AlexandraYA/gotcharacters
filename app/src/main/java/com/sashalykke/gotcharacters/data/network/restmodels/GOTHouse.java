package com.sashalykke.gotcharacters.data.network.restmodels;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GOTHouse {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("words")
    @Expose
    private String words;
    @SerializedName("swornMembers")
    @Expose
    private List<String> swornMembers = new ArrayList<String>();

    /**
     * 
     * @return
     *     The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The words
     */
    public String getWords() {
        return words;
    }

    /**
     * 
     * @param words
     *     The words
     */
    public void setWords(String words) {
        this.words = words;
    }

    /**
     * 
     * @return
     *     The swornMembers
     */
    public List<String> getSwornMembers() {
        return swornMembers;
    }

    /**
     * 
     * @param swornMembers
     *     The swornMembers
     */
    public void setSwornMembers(List<String> swornMembers) {
        this.swornMembers = swornMembers;
    }

}
