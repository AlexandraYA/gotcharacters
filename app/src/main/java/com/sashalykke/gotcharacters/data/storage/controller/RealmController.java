package com.sashalykke.gotcharacters.data.storage.controller;


import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import com.sashalykke.gotcharacters.data.storage.models.House;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }

    //clear all objects from Book.class
    public void clearAll() {

        realm.beginTransaction();
        realm.clear(House.class);
        realm.commitTransaction();
    }

    //find all objects in the Book.class
    public RealmResults<House> getHouses() {

        return realm.where(House.class).findAll();
    }

    //query a single item with the given id
    public House getHouse(String id) {

        return realm.where(House.class).equalTo("id", id).findFirst();
    }

    //check if Book.class is empty
    public boolean hasHouses() {

        return !realm.allObjects(House.class).isEmpty();
    }

    //query example
    public RealmResults<House> queryedHouses() {

        return realm.where(House.class)
                .contains("author", "Author 0")
                .or()
                .contains("title", "Realm")
                .findAll();

    }
}
