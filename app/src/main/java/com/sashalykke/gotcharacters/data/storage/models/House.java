package com.sashalykke.gotcharacters.data.storage.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class House extends RealmObject {

    @PrimaryKey
    private String id;
    private String url;
    private String name;
    private String words;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }
}
